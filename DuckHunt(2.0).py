import pygame
import os
import random

pygame.init()
pygame.mouse.set_visible(False)
size = width, height = 1280, 769
screen = pygame.display.set_mode(size)
pos = 1280, 769
FPS = 60
clock = pygame.time.Clock()
STEP = 10
duck_sprite = pygame.sprite.Group()


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error as message:
        print('Cannot load image:', name)
        raise SystemExit(message)
    image = image.convert_alpha()

    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey)
    return image


def start_screen():
    intro_text = ["Duck hunt", "",
                  "Правила игры",
                  "Убейте утку,",
                  "Удачи"]

    fon = pygame.transform.scale(load_image('fon.png'), (width, height))
    screen.blit(fon, (0, 0))
    font = pygame.font.Font(None, 30)
    text_coord = 50
    for line in intro_text:
        string_rendered = font.render(line, 1, pygame.Color('black'))
        intro_rect = string_rendered.get_rect()
        text_coord += 10
        intro_rect.top = text_coord
        intro_rect.x = 10
        text_coord += intro_rect.height
        screen.blit(string_rendered, intro_rect)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                return  # начинаем игру
        pygame.display.flip()
        clock.tick(FPS)


class Cursor(pygame.sprite.Sprite):
    image = load_image("cursor.png")

    def __init__(self, group):
        super().__init__(group)
        self.image = Cursor.image
        self.rect = self.image.get_rect()

    def update(self):
        self.rect = pos


class Fon(pygame.sprite.Sprite):
    image = load_image("fon.png")

    def __init__(self, group):
        super().__init__(group)
        self.image = Fon.image
        self.rect = self.image.get_rect()

    def update(self):
        self.image = Fon.win_image


class DuckSprite(pygame.sprite.Sprite):
    image = load_image("duck.png")

    def __init__(self, sheet, columns, rows, x, y):
        super().__init__(duck_sprite)
        self.frames = []
        self.cut_sheet(sheet, columns, rows)
        self.cur_frame = 0
        self.image = self.frames[self.cur_frame]
        self.rect = self.rect.move(x, y)
        self.x = self.rect[0]
        self.y = self.rect[1]

    def cut_sheet(self, sheet, columns, rows):
        self.rect = pygame.Rect(0, 0, sheet.get_width() // columns, sheet.get_height() // rows)
        for j in range(rows):
            for i in range(columns):
                frame_location = (self.rect.w * i, self.rect.h * j)
                self.frames.append(sheet.subsurface(pygame.Rect(frame_location, self.rect.size)))

    def update(self):
        self.cur_frame = (self.cur_frame + 1) % len(self.frames)
        self.image = self.frames[self.cur_frame]
        if random.randint(0, 3) == 0:
            self.rect = self.rect[0] - STEP, self.rect[1]
        elif random.randint(0, 3) == 1:
            self.rect = self.rect[0] + STEP, self.rect[1]
        elif random.randint(0, 3) == 2:
            self.rect = self.rect[0], self.rect[1] - STEP
        elif random.randint(0, 3) == 3:
            self.rect = self.rect[0], self.rect[1] + STEP
        clock.tick(FPS)

    def kill(self):
        rect = pygame.Rect(self.rect, (38, 41))
        if rect.collidepoint(pos):
            duck_sprite.remove(duck)


cursor = pygame.sprite.Group()
back_ground = pygame.sprite.Group()
Cursor(cursor)
Fon(back_ground)
start_screen()
duck = DuckSprite(load_image("duck.png"), 3, 1, 600, 350)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            duck.kill()
    screen.fill(pygame.Color("white"))
    back_ground.draw(screen)
    pos = pygame.mouse.get_pos()
    if pygame.mouse.get_focused():
        cursor.draw(screen)
    duck_sprite.draw(screen)
    duck_sprite.update()
    cursor.update()
    pygame.display.flip()

pygame.quit()
